import React from "react";
// import ReactDOM from 'react-dom';
import './index.css';

// EL VIDEO ES
// Full Stack Food Delivery App - React Redux Reducer, Firebase _  For Beginners

import { StrictMode } from "react";
import { createRoot } from "react-dom/client";

import { BrowserRouter as Router } from "react-router-dom";

import App from "./App";
import { StateProvider } from "./context/StateProvider";
import { initialState } from "./context/initialState";
import reducer from './context/reducer'



const rootElement = document.getElementById("root");
const root = createRoot(rootElement);

root.render(
  <StrictMode>
    <Router>
    <StateProvider initialState={initialState} reducer={reducer}>
      <App />
    </StateProvider>
  </Router>
  </StrictMode>
);

// ReactDOM.render(
//   <Router>
//     <StateProvider initialState={initialState} reducer={reducer}>
//       <App />
//     </StateProvider>
//   </Router>,
//   document.getElementById("root")
// );