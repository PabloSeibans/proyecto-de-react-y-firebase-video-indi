import {getApp, getApps, initializeApp} from 'firebase/app';
import {getFirestore } from 'firebase/firestore';
import {getStorage} from 'firebase/storage';


const firebaseConfig = {
  apiKey: "AIzaSyDS9rmz5Fl7cgictMAv4BDYs9T5X9lf70c",
  authDomain: "adelart-project.firebaseapp.com",
  databaseURL: "https://adelart-project-default-rtdb.firebaseio.com",
  projectId: "adelart-project",
  storageBucket: "adelart-project.appspot.com",
  messagingSenderId: "582799635543",
  appId: "1:582799635543:web:c2e6ac00764e7e574f1f12"
};

const app = getApps.length > 0 ? getApp(): initializeApp(firebaseConfig);

const firestore = getFirestore(app);
const storage = getStorage(app);

export {app, firestore, storage};